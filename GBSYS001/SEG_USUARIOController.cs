﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GBSYS001.Data;
using GBSYS001.Models;

namespace GBSYS001
{
    public class SEG_USUARIOController : Controller
    {
        private readonly GBSYS001Context _context;

        public SEG_USUARIOController(GBSYS001Context context)
        {
            _context = context;
        }

        // GET: SEG_USUARIO
        public async Task<IActionResult> Index()
        {
            return View(await _context.SEG_USUARIO.ToListAsync());
        }

        // GET: SEG_USUARIO/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var sEG_USUARIO = await _context.SEG_USUARIO
                .FirstOrDefaultAsync(m => m.ID_USUARIO == id);
            if (sEG_USUARIO == null)
            {
                return NotFound();
            }

            return View(sEG_USUARIO);
        }

        // GET: SEG_USUARIO/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: SEG_USUARIO/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID_USUARIO,NOMBRE,PRIMER_APELLIDO,SEGUNDO_APELLIDO,CEDULA,CONTRASENA,ESTADO,CORREO")] SEG_USUARIO sEG_USUARIO)
        {
            if (ModelState.IsValid)
            {
                _context.Add(sEG_USUARIO);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(sEG_USUARIO);
        }

        // GET: SEG_USUARIO/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var sEG_USUARIO = await _context.SEG_USUARIO.FindAsync(id);
            if (sEG_USUARIO == null)
            {
                return NotFound();
            }
            return View(sEG_USUARIO);
        }

        // POST: SEG_USUARIO/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("ID_USUARIO,NOMBRE,PRIMER_APELLIDO,SEGUNDO_APELLIDO,CEDULA,CONTRASENA,ESTADO,CORREO")] SEG_USUARIO sEG_USUARIO)
        {
            if (id != sEG_USUARIO.ID_USUARIO)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(sEG_USUARIO);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SEG_USUARIOExists(sEG_USUARIO.ID_USUARIO))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(sEG_USUARIO);
        }

        // GET: SEG_USUARIO/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var sEG_USUARIO = await _context.SEG_USUARIO
                .FirstOrDefaultAsync(m => m.ID_USUARIO == id);
            if (sEG_USUARIO == null)
            {
                return NotFound();
            }

            return View(sEG_USUARIO);
        }

        // POST: SEG_USUARIO/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var sEG_USUARIO = await _context.SEG_USUARIO.FindAsync(id);
            _context.SEG_USUARIO.Remove(sEG_USUARIO);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SEG_USUARIOExists(string id)
        {
            return _context.SEG_USUARIO.Any(e => e.ID_USUARIO == id);
        }
    }
}
