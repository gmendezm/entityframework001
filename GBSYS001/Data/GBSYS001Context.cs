﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using GBSYS001.Models;

namespace GBSYS001.Data
{
    public class GBSYS001Context : DbContext
    {
        public GBSYS001Context (DbContextOptions<GBSYS001Context> options)
            : base(options)
        {
        }

        public DbSet<GBSYS001.Models.SEG_USUARIO> SEG_USUARIO { get; set; }
    }
}
