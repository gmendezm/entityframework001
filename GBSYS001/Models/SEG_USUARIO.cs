﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GBSYS001.Models
{
    public class SEG_USUARIO
    {
        [Key]
        public string ID_USUARIO { get; set; }
        public string NOMBRE { get; set; }
        public string PRIMER_APELLIDO { get; set; }
        public string SEGUNDO_APELLIDO { get; set; }
        public string CEDULA { get; set; }
        public string CONTRASENA { get; set; }
        public bool ESTADO { get; set; }
        public string CORREO { get; set; }

    }
}
