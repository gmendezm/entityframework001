﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GBSYS002.Models.EF
{
    public partial class SegRolPerfil
    {
        public int IdPerfil { get; set; }
        public int IdRol { get; set; }
        public int IdRolPerfil { get; set; }

        public virtual SegPerfil IdPerfilNavigation { get; set; }
        public virtual SegRol IdRolNavigation { get; set; }
    }
}
