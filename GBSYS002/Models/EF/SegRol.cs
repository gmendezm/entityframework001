﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GBSYS002.Models.EF
{
    public partial class SegRol
    {
        public SegRol()
        {
            SegRolPerfils = new HashSet<SegRolPerfil>();
        }

        public int IdRol { get; set; }
        public string DscRol { get; set; }
        public int? IdOpcion { get; set; }

        public virtual SegOpcionesMenu IdOpcionNavigation { get; set; }
        public virtual ICollection<SegRolPerfil> SegRolPerfils { get; set; }
    }
}
