﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GBSYS002.Models.EF
{
    public partial class SegPerfil
    {
        public SegPerfil()
        {
            SegRolPerfils = new HashSet<SegRolPerfil>();
            SegUsuarioPerfils = new HashSet<SegUsuarioPerfil>();
        }

        public int IdPerfil { get; set; }
        public string DscPerfil { get; set; }
        public bool? Estado { get; set; }

        public virtual ICollection<SegRolPerfil> SegRolPerfils { get; set; }
        public virtual ICollection<SegUsuarioPerfil> SegUsuarioPerfils { get; set; }
    }
}
