﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GBSYS002.Models.EF
{
    public partial class SegAuditorium
    {
        public string Nivel { get; set; }
        public DateTime Fecha { get; set; }
        public string InicioSesion { get; set; }
        public string Usuario { get; set; }
        public string Accion { get; set; }
        public string Objeto { get; set; }
        public string Sentencia { get; set; }
        public string DetalleEvento { get; set; }
    }
}
