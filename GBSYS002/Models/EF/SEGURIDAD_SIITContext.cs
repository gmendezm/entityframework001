﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace GBSYS002.Models.EF
{
    public partial class SEGURIDAD_SIITContext : DbContext
    {
        public SEGURIDAD_SIITContext()
        {
        }

        public SEGURIDAD_SIITContext(DbContextOptions<SEGURIDAD_SIITContext> options)
            : base(options)
        {
        }

        public virtual DbSet<SegAuditorium> SegAuditoria { get; set; }
        public virtual DbSet<SegBitacora> SegBitacoras { get; set; }
        public virtual DbSet<SegModulo> SegModulos { get; set; }
        public virtual DbSet<SegOpcionesMenu> SegOpcionesMenus { get; set; }
        public virtual DbSet<SegPerfil> SegPerfils { get; set; }
        public virtual DbSet<SegRol> SegRols { get; set; }
        public virtual DbSet<SegRolPerfil> SegRolPerfils { get; set; }
        public virtual DbSet<SegSistema> SegSistemas { get; set; }
        public virtual DbSet<SegUsuario> SegUsuarios { get; set; }
        public virtual DbSet<SegUsuarioPerfil> SegUsuarioPerfils { get; set; }
        public virtual DbSet<SmcPersonaTse> SmcPersonaTses { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            /*if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Data Source=LAPTOP-GMENDEZM;Initial Catalog=SEGURIDAD_SIIT;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            }*/
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Modern_Spanish_CI_AS");

            modelBuilder.Entity<SegAuditorium>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("SEG_AUDITORIA");

                entity.Property(e => e.Accion)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.DetalleEvento)
                    .IsRequired()
                    .HasColumnType("xml");

                entity.Property(e => e.Fecha)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.InicioSesion)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Nivel)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.Objeto)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Sentencia)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.Usuario)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SegBitacora>(entity =>
            {
                entity.HasKey(e => e.IdBitacora)
                    .HasName("pk_SEG_BITACORA");

                entity.ToTable("SEG_BITACORA");

                entity.Property(e => e.IdBitacora).HasColumnName("ID_BITACORA");

                entity.Property(e => e.DscBitacora).HasColumnName("DSC_BITACORA");

                entity.Property(e => e.FecBitacora)
                    .HasColumnType("datetime")
                    .HasColumnName("FEC_BITACORA");

                entity.Property(e => e.IdSistema).HasColumnName("ID_SISTEMA");

                entity.Property(e => e.IdUsuario)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("ID_USUARIO");

                entity.Property(e => e.Movimiento)
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasColumnName("MOVIMIENTO");
            });

            modelBuilder.Entity<SegModulo>(entity =>
            {
                entity.HasKey(e => e.IdModulo)
                    .HasName("pk_SEG_MODULO");

                entity.ToTable("SEG_MODULO");

                entity.Property(e => e.IdModulo).HasColumnName("ID_MODULO");

                entity.Property(e => e.DscModulo)
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasColumnName("DSC_MODULO");

                entity.Property(e => e.Estado).HasColumnName("ESTADO");

                entity.Property(e => e.IdSistema).HasColumnName("ID_SISTEMA");

                entity.Property(e => e.Inicial)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("INICIAL");

                entity.HasOne(d => d.IdSistemaNavigation)
                    .WithMany(p => p.SegModulos)
                    .HasForeignKey(d => d.IdSistema)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SISTEMA_MODULO");
            });

            modelBuilder.Entity<SegOpcionesMenu>(entity =>
            {
                entity.HasKey(e => e.IdOpcion)
                    .HasName("pk_SEG_OPCIONES_MENU");

                entity.ToTable("SEG_OPCIONES_MENU");

                entity.HasIndex(e => e.IdOpcion, "UQ__SEG_OPCI__34D506621273C1CD")
                    .IsUnique();

                entity.Property(e => e.IdOpcion).HasColumnName("ID_OPCION");

                entity.Property(e => e.DscOpcion)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasColumnName("DSC_OPCION");

                entity.Property(e => e.IdMenu)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("ID_MENU");

                entity.Property(e => e.IdModulo).HasColumnName("ID_MODULO");

                entity.Property(e => e.IdPadre)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("ID_PADRE");

                entity.Property(e => e.OrdenMenu).HasColumnName("ORDEN_MENU");

                entity.HasOne(d => d.IdModuloNavigation)
                    .WithMany(p => p.SegOpcionesMenus)
                    .HasForeignKey(d => d.IdModulo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Modulo_Opciones");
            });

            modelBuilder.Entity<SegPerfil>(entity =>
            {
                entity.HasKey(e => e.IdPerfil)
                    .HasName("pk_SEG_PERFIL");

                entity.ToTable("SEG_PERFIL");

                entity.Property(e => e.IdPerfil).HasColumnName("ID_PERFIL");

                entity.Property(e => e.DscPerfil)
                    .HasMaxLength(300)
                    .IsUnicode(false)
                    .HasColumnName("DSC_PERFIL");

                entity.Property(e => e.Estado).HasColumnName("ESTADO");
            });

            modelBuilder.Entity<SegRol>(entity =>
            {
                entity.HasKey(e => e.IdRol)
                    .HasName("pk_SEG_ROL");

                entity.ToTable("SEG_ROL");

                entity.Property(e => e.IdRol).HasColumnName("ID_ROL");

                entity.Property(e => e.DscRol)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("DSC_ROL");

                entity.Property(e => e.IdOpcion).HasColumnName("ID_OPCION");

                entity.HasOne(d => d.IdOpcionNavigation)
                    .WithMany(p => p.SegRols)
                    .HasForeignKey(d => d.IdOpcion)
                    .HasConstraintName("FK_OPCION_ROL");
            });

            modelBuilder.Entity<SegRolPerfil>(entity =>
            {
                entity.HasKey(e => e.IdRolPerfil)
                    .HasName("pk_SEG_ROL_PERFIL");

                entity.ToTable("SEG_ROL_PERFIL");

                entity.HasIndex(e => e.IdRolPerfil, "UQ__SEG_ROL___B19BEE8C15502E78")
                    .IsUnique();

                entity.Property(e => e.IdRolPerfil).HasColumnName("ID_ROL_PERFIL");

                entity.Property(e => e.IdPerfil).HasColumnName("ID_PERFIL");

                entity.Property(e => e.IdRol).HasColumnName("ID_ROL");

                entity.HasOne(d => d.IdPerfilNavigation)
                    .WithMany(p => p.SegRolPerfils)
                    .HasForeignKey(d => d.IdPerfil)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PERFIL_ROL");

                entity.HasOne(d => d.IdRolNavigation)
                    .WithMany(p => p.SegRolPerfils)
                    .HasForeignKey(d => d.IdRol)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ROL_PERFIL");
            });

            modelBuilder.Entity<SegSistema>(entity =>
            {
                entity.HasKey(e => e.IdSistema)
                    .HasName("pk_SEG_SISTEMA");

                entity.ToTable("SEG_SISTEMA");

                entity.Property(e => e.IdSistema).HasColumnName("ID_SISTEMA");

                entity.Property(e => e.DscSistema)
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasColumnName("DSC_SISTEMA");

                entity.Property(e => e.Estado).HasColumnName("ESTADO");
            });

            modelBuilder.Entity<SegUsuario>(entity =>
            {
                entity.HasKey(e => e.IdUsuario)
                    .HasName("pk_USUARIO");

                entity.ToTable("SEG_USUARIO");

                entity.Property(e => e.IdUsuario)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("ID_USUARIO");

                entity.Property(e => e.Cedula)
                    .HasMaxLength(15)
                    .HasColumnName("CEDULA");

                entity.Property(e => e.Contrasena).HasColumnName("CONTRASENA");

                entity.Property(e => e.Correo)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("CORREO");

                entity.Property(e => e.EsExterno).HasColumnName("ES_EXTERNO");

                entity.Property(e => e.EstaSesion).HasColumnName("ESTA_SESION");

                entity.Property(e => e.Estado).HasColumnName("ESTADO");

                entity.Property(e => e.Nombre)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("NOMBRE");

                entity.Property(e => e.PrimerApellido)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("PRIMER_APELLIDO");

                entity.Property(e => e.SegundoApellido)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("SEGUNDO_APELLIDO");
            });

            modelBuilder.Entity<SegUsuarioPerfil>(entity =>
            {
                entity.HasKey(e => e.IdUsuarioPerfil)
                    .HasName("pk_SEG_USUARIO_PERFIL");

                entity.ToTable("SEG_USUARIO_PERFIL");

                entity.HasIndex(e => e.IdUsuarioPerfil, "UQ__SEG_USUA__0704C0CE182C9B23")
                    .IsUnique();

                entity.Property(e => e.IdUsuarioPerfil).HasColumnName("ID_USUARIO_PERFIL");

                entity.Property(e => e.IdPerfil).HasColumnName("ID_PERFIL");

                entity.Property(e => e.IdUsuario)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("ID_USUARIO");

                entity.HasOne(d => d.IdPerfilNavigation)
                    .WithMany(p => p.SegUsuarioPerfils)
                    .HasForeignKey(d => d.IdPerfil)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PERFIL_USUARIO");

                entity.HasOne(d => d.IdUsuarioNavigation)
                    .WithMany(p => p.SegUsuarioPerfils)
                    .HasForeignKey(d => d.IdUsuario)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_USUARIO_PERFIL");
            });

            modelBuilder.Entity<SmcPersonaTse>(entity =>
            {
                entity.HasKey(e => e.Cedula)
                    .HasName("pk_SMC_PERSONA_TSE");

                entity.ToTable("SMC_PERSONA_TSE");

                entity.Property(e => e.Cedula)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("CEDULA");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasColumnName("NOMBRE");

                entity.Property(e => e.Primerapellido)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasColumnName("PRIMERAPELLIDO");

                entity.Property(e => e.Segundoapellido)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasColumnName("SEGUNDOAPELLIDO");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
