﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GBSYS002.Models.EF
{
    public partial class SegUsuario
    {
        public SegUsuario()
        {
            SegUsuarioPerfils = new HashSet<SegUsuarioPerfil>();
        }

        public string IdUsuario { get; set; }
        public string Nombre { get; set; }
        public string PrimerApellido { get; set; }
        public string SegundoApellido { get; set; }
        public bool Estado { get; set; }
        public bool? EsExterno { get; set; }
        public string Contrasena { get; set; }
        public string Cedula { get; set; }
        public bool EstaSesion { get; set; }
        public string Correo { get; set; }

        public virtual ICollection<SegUsuarioPerfil> SegUsuarioPerfils { get; set; }
    }
}
