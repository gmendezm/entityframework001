﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GBSYS002.Models.EF
{
    public partial class SegModulo
    {
        public SegModulo()
        {
            SegOpcionesMenus = new HashSet<SegOpcionesMenu>();
        }

        public int IdSistema { get; set; }
        public int IdModulo { get; set; }
        public string DscModulo { get; set; }
        public string Inicial { get; set; }
        public bool? Estado { get; set; }

        public virtual SegSistema IdSistemaNavigation { get; set; }
        public virtual ICollection<SegOpcionesMenu> SegOpcionesMenus { get; set; }
    }
}
