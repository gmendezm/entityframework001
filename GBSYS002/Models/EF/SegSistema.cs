﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GBSYS002.Models.EF
{
    public partial class SegSistema
    {
        public SegSistema()
        {
            SegModulos = new HashSet<SegModulo>();
        }

        public int IdSistema { get; set; }
        public string DscSistema { get; set; }
        public bool? Estado { get; set; }

        public virtual ICollection<SegModulo> SegModulos { get; set; }
    }
}
