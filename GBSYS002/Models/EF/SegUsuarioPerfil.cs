﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GBSYS002.Models.EF
{
    public partial class SegUsuarioPerfil
    {
        public string IdUsuario { get; set; }
        public int IdPerfil { get; set; }
        public int IdUsuarioPerfil { get; set; }

        public virtual SegPerfil IdPerfilNavigation { get; set; }
        public virtual SegUsuario IdUsuarioNavigation { get; set; }
    }
}
