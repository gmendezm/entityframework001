﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GBSYS002.Models.EF
{
    public partial class SegOpcionesMenu
    {
        public SegOpcionesMenu()
        {
            SegRols = new HashSet<SegRol>();
        }

        public int IdModulo { get; set; }
        public int IdOpcion { get; set; }
        public string DscOpcion { get; set; }
        public int OrdenMenu { get; set; }
        public string IdPadre { get; set; }
        public string IdMenu { get; set; }

        public virtual SegModulo IdModuloNavigation { get; set; }
        public virtual ICollection<SegRol> SegRols { get; set; }
    }
}
