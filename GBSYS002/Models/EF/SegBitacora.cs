﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GBSYS002.Models.EF
{
    public partial class SegBitacora
    {
        public int IdBitacora { get; set; }
        public string Movimiento { get; set; }
        public string DscBitacora { get; set; }
        public DateTime? FecBitacora { get; set; }
        public int? IdSistema { get; set; }
        public string IdUsuario { get; set; }
    }
}
