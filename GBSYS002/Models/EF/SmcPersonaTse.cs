﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GBSYS002.Models.EF
{
    public partial class SmcPersonaTse
    {
        public string Cedula { get; set; }
        public string Nombre { get; set; }
        public string Primerapellido { get; set; }
        public string Segundoapellido { get; set; }
    }
}
