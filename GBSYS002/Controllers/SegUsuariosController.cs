using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using GBSYS002.Models.EF;

namespace GBSYS002.Controllers
{
    [Route("api/[controller]/[action]")]
    public class SegUsuariosController : Controller
    {
        private SEGURIDAD_SIITContext _context;

        public SegUsuariosController(SEGURIDAD_SIITContext context) {
            _context = context;
        }

        [HttpGet]
        public async Task<IActionResult> Get(DataSourceLoadOptions loadOptions) {
            var segusuarios = _context.SegUsuarios.Select(i => new {
                i.IdUsuario,
                i.Nombre,
                i.PrimerApellido,
                i.SegundoApellido,
                i.Estado,
                i.EsExterno,
                i.Contrasena,
                i.Cedula,
                i.EstaSesion,
                i.Correo
            });

            // If you work with a large amount of data, consider specifying the PaginateViaPrimaryKey and PrimaryKey properties.
            // In this case, keys and data are loaded in separate queries. This can make the SQL execution plan more efficient.
            // Refer to the topic https://github.com/DevExpress/DevExtreme.AspNet.Data/issues/336.
            // loadOptions.PrimaryKey = new[] { "IdUsuario" };
            // loadOptions.PaginateViaPrimaryKey = true;

            return Json(await DataSourceLoader.LoadAsync(segusuarios, loadOptions));
        }

        [HttpPost]
        public async Task<IActionResult> Post(string values) {
            var model = new SegUsuario();
            var valuesDict = JsonConvert.DeserializeObject<IDictionary>(values);
            PopulateModel(model, valuesDict);

            if(!TryValidateModel(model))
                return BadRequest(GetFullErrorMessage(ModelState));

            var result = _context.SegUsuarios.Add(model);
            await _context.SaveChangesAsync();

            return Json(new { result.Entity.IdUsuario });
        }

        [HttpPut]
        public async Task<IActionResult> Put(string key, string values) {
            var model = await _context.SegUsuarios.FirstOrDefaultAsync(item => item.IdUsuario == key);
            if(model == null)
                return StatusCode(409, "Object not found");

            var valuesDict = JsonConvert.DeserializeObject<IDictionary>(values);
            PopulateModel(model, valuesDict);

            if(!TryValidateModel(model))
                return BadRequest(GetFullErrorMessage(ModelState));

            await _context.SaveChangesAsync();
            return Ok();
        }

        [HttpDelete]
        public async Task Delete(string key) {
            var model = await _context.SegUsuarios.FirstOrDefaultAsync(item => item.IdUsuario == key);

            _context.SegUsuarios.Remove(model);
            await _context.SaveChangesAsync();
        }


        private void PopulateModel(SegUsuario model, IDictionary values) {
            string ID_USUARIO = nameof(SegUsuario.IdUsuario);
            string NOMBRE = nameof(SegUsuario.Nombre);
            string PRIMER_APELLIDO = nameof(SegUsuario.PrimerApellido);
            string SEGUNDO_APELLIDO = nameof(SegUsuario.SegundoApellido);
            string ESTADO = nameof(SegUsuario.Estado);
            string ES_EXTERNO = nameof(SegUsuario.EsExterno);
            string CONTRASENA = nameof(SegUsuario.Contrasena);
            string CEDULA = nameof(SegUsuario.Cedula);
            string ESTA_SESION = nameof(SegUsuario.EstaSesion);
            string CORREO = nameof(SegUsuario.Correo);

            if(values.Contains(ID_USUARIO)) {
                model.IdUsuario = Convert.ToString(values[ID_USUARIO]);
            }

            if(values.Contains(NOMBRE)) {
                model.Nombre = Convert.ToString(values[NOMBRE]);
            }

            if(values.Contains(PRIMER_APELLIDO)) {
                model.PrimerApellido = Convert.ToString(values[PRIMER_APELLIDO]);
            }

            if(values.Contains(SEGUNDO_APELLIDO)) {
                model.SegundoApellido = Convert.ToString(values[SEGUNDO_APELLIDO]);
            }

            if(values.Contains(ESTADO)) {
                model.Estado = Convert.ToBoolean(values[ESTADO]);
            }

            if(values.Contains(ES_EXTERNO)) {
                model.EsExterno = values[ES_EXTERNO] != null ? Convert.ToBoolean(values[ES_EXTERNO]) : (bool?)null;
            }

            if(values.Contains(CONTRASENA)) {
                model.Contrasena = Convert.ToString(values[CONTRASENA]);
            }

            if(values.Contains(CEDULA)) {
                model.Cedula = Convert.ToString(values[CEDULA]);
            }

            if(values.Contains(ESTA_SESION)) {
                model.EstaSesion = Convert.ToBoolean(values[ESTA_SESION]);
            }

            if(values.Contains(CORREO)) {
                model.Correo = Convert.ToString(values[CORREO]);
            }
        }

        private string GetFullErrorMessage(ModelStateDictionary modelState) {
            var messages = new List<string>();

            foreach(var entry in modelState) {
                foreach(var error in entry.Value.Errors)
                    messages.Add(error.ErrorMessage);
            }

            return String.Join(" ", messages);
        }
    }
}